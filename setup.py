# -*- coding: utf8-*-
from setuptools import setup
import sys

ver_dic = {}
version_file = open("version.py")
try:
    version_file_contents = version_file.read()
finally:
    version_file.close()

exec(compile(version_file_contents, "version.py", 'exec'), ver_dic)

build_exe_options = {
    "packages": ["os"],
    # "excludes": ["tkinter"],
    "compressed": True
}

base = None
options = None
executables = None

setup(
    name="fmrifixer",
    version=ver_dic["VERSION_TEXT"],
    description="fix those dammned rs-fMRI-files from our INSPIREmcDESPOT-study",
    author="Hannes Wahl",
    author_email="hannes.wahl@uniklinikum-dresden.de",
    setup_requires=['setuptools>=18.0', ],
    install_requires=['numpy', 'nibabel'],
    packages = [''],
    entry_points={
        'console_scripts': ['fmrifixer=fmrifixer:main'],
    },
    options=options,
    executables=executables
)