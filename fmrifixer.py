#!/usr/bin/env python3

import os
import numpy as np
import nibabel as nib


def fix(infile, outfile, shift = 170.0, justflip = False):
    if os.path.exists(infile):
        img_org = nib.load(infile)
    else:
        raise IOError("#0: Input file does not exists!")

    header = img_org.get_header()
    # first check, if z-value is low enough to be needed to fix:
    qform = header.get_qform()
    if qform[2][3] < -shift or justflip:
        # shift z-axis 170mm up, to reduce misaligment to anatomical data
        if not justflip:
            qform[2][3] += shift
        header.set_qform(qform)

        # set sform to 0, so it is not used
        header['sform_code'] = 0

        data = img_org.get_data()
        # flip reverse stored data in z-direction
        data_flip = np.flip(data, 2)
        # use qform as affine to get the alignment right, otherwise the old affine and new qform would confuse every programm
        if not outfile:
            outfile = infile.replace('.nii', '_fixed.nii')
        nib.save(nib.Nifti1Image(data_flip, affine=qform, header=header), outfile)
    else:
        print("data seems to be fine, didn't change anything...")

def main():
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--infile', required=True, help="input-file")
    parser.add_argument('-o', '--outfile', help="output-file")
    parser.add_argument('-s', '--shift', default=170.0, type=float, help="shift z-plane X mm, default: 170")
    parser.add_argument('-f', '--justflip', action="store_true", help="only flip data in z-direction")

    args = parser.parse_args()

    fix(args.infile, args.outfile, args.shift, args.justflip)
    print('Finished.')


if __name__ == "__main__":
    main()



